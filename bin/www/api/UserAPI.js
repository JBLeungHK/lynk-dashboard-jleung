(function () {
	"use strict";

	var ng = angular.module('api.user', ['utilities.restful.api',
		'helper.userData'
	]);

	ng.factory('userAPI', function ($http, APIRoute, RestfulManager,
		UserDataHelper) {

		var apiPrefix = APIRoute.url + 'user/';

		function createHeaderObj(dataObj) {
			var headerObj = null;
			if (dataObj) {
				headerObj = dataObj;
			} else {
				headerObj = {};
			}
			headerObj['lynk-access-token'] = UserDataHelper.getToken();
			return headerObj;
		}


		//implement api
		function api_get_userlistByUserID(queryObj, successCallback, errorCallback) {
			var url = apiPrefix + 'getusers';
			var headerData = createHeaderObj();
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};
			RestfulManager.get().url(url).header(headerData).queryParams(queryObj).requestSend(
				apiCallback,
				failCallback);
		}

		//interface for api call
		return {
			getUserlistByUserID: function (ids, successCallback) {
				var queryObj = {
					'ids': ids.join(",")
				};
				api_get_userlistByUserID(queryObj, successCallback);
			}
		};
	});
})();
