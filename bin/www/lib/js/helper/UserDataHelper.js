(function() {

  //private variables
  var accessTokenKey = "access_token";
  var userNameKey = "user_name";

  var ng = angular.module('helper.userData', ['utilities.localStorage']);

  ng.factory('UserDataHelper', function(LocalStorageHelper) {


    var userDataKey = LocalStorageHelper.getUserDataKey();
    var userData = (LocalStorageHelper.getRecord(userDataKey) || LocalStorageHelper.setRecord(userDataKey, {}));

    //private function
    function getValue(key) {
      return LocalStorageHelper.getValueFromRecord(userDataKey, key);
    }

    function addValue(key, value) {
      LocalStorageHelper.addValueToRecord(userDataKey, key, value);
    }

    function clearValues(keyArray) {
      angular.forEach(keyArray, function(key) {
        LocalStorageHelper.addValueToRecord(userDataKey, key, '');
      });
    }
    //public function
    return {
      getToken: function() {
        return getValue(accessTokenKey);
      },
      setToken: function(token) {
        addValue(accessTokenKey, token);
      },
      getUserName: function() {
        return getValue(userNameKey);
      },
      setUserName: function(userName) {
        addValue(userNameKey, userName);
      },
      getAll: function() {
        return LocalStorageHelper.getRecord(userDataKey);
      },
      clearAll: function() {
        clearValues([accessTokenKey, userNameKey]);
      }
    };

  });
}());
