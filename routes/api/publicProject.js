/*jslint node: true */
"use strict";

var express = require('express');
var extend = require('util')._extend;
var projectModel = require('../../lib/js/model/projectModel');
var userModel = require('../../lib/js/model/userModel');

var router = express.Router();

router.get('/get', function (req, res, next) {
	var id = req.query.id;

	var callback = function (data) {
		console.log("checking in detailList");
		console.log(data);
		var useridList = [];
		if(data !== undefined && data.decision_list){
			data.decision_list.forEach(function (value) {
				useridList.push(value.user_id);
			});
			console.log(useridList);

			if (useridList.length > 0) {
				var BreakException = {};
				userModel.getUserlist(useridList, function (userList, status) {
					var updateDecisionList = [];
					useridList.forEach(function (userID) {
						var tempValue = {};
						try {
							data.decision_list.forEach(function (value) {
								if (value.user_id === userID) {
									tempValue.decision = value.decision;
									tempValue.user_id = value.user_id;
									tempValue._id = value._id;
									throw BreakException;
								}
							});
						} catch (e) {
							if (e !== BreakException) throw e;
						}
						try {
							userList.forEach(function (value) {
								if (value.user_id === userID) {
									tempValue.user_name = value.name;
									tempValue.description = value.description;
									throw BreakException;
								}
							});
						} catch (e) {
							if (e !== BreakException) throw e;
						}
						updateDecisionList.push(tempValue);
					});
					var result = data;
					result.decision_list = updateDecisionList;
					res.json(result);
				});
			} else {
				res.send(null);
			}
		}else{
			res.send(null);
		}
	};
	projectModel.getProjectByID(id, callback);
});



module.exports = router;
