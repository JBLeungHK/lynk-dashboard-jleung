/*jslint node: true */
"use strict";


var jwt = require('jsonwebtoken');

var action = {};
var privateKey = 'lynkdashboardPrivateKey';


function getTokenFromRequest(req) {
	return req.headers['lynk-access-token'];
}

function checkToken(token, req, res, next, callback) {
	jwt.verify(token, privateKey, callback);
}


action.creatToken = function (payload) {
	var options = {
		 expiresIn: '16d'
	};
	return jwt.sign(payload, privateKey, options);
};


action.jwtVerify = function (req, res, next) {
	// check header or url parameters or post parameters for token
	// decode token
	var token = getTokenFromRequest(req);
	if (token) {
		// verifies secret and checks exp
		var callback = function (err, decoded) {
			if (err) {
				return res.status(200).send({
					success: false,
					message: 'Token not valid.'
				});
			} else {
				return res.status(200).send({
					success: true,
					message: 'valid token'
				});
			}
		};
		checkToken(token, req, res, next, callback);

	} else {

		// if there is no token
		// return an error
		return res.status(403).send({
			success: false,
			message: 'No token provided.'
		});

	}
};
action.jwtVerifyToNext = function (req, res, next) {
	// check header or url parameters or post parameters for token
	// decode token
	var token = getTokenFromRequest(req);
	if (token) {
		// verifies secret and checks exp
		var callback = function (err, decoded) {
			if (err) {
				return res.send(false);
			} else {
				// if everything is good, save to request for use in other routes
				req.decoded = decoded;
				next();
			}
		};
		checkToken(token, req, res, next, callback);

	} else {
		// if there is no token
		// return an error
		return res.status(403).send({
			success: false,
			message: 'No token provided.'
		});

	}
};

module.exports = action;
