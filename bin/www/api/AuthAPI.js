(function () {
	"use strict";

	var ng = angular.module('api.auth', ['utilities.restful.api']);


	ng.factory('authAPI', function ($http, APIRoute, RestfulManager) {

		var apiPrefix = APIRoute.url + 'auth/';

		function api_get_tokenValid(token, successCallback, errorCallback) {
			var url = apiPrefix + 'checkToken';
			var data = {
				'lynk-access-token': token
			};
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};

			RestfulManager.get().url(url).header(data).requestSend(
				apiCallback,
				failCallback);
		}

		function api_post_login(loginObj, successCallback, errorCallback) {
			var url = apiPrefix + 'Login';
			var data = loginObj;
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};

			RestfulManager.post().url(url).header(data).requestSend(
				apiCallback,
				failCallback);
		}

		function api_put_register(regObj, successCallback, errorCallback) {
			var url = apiPrefix + 'createUser';
			var data = regObj;
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};

			RestfulManager.put().url(url).header(data).requestSend(
				apiCallback,
				failCallback);
		}

		return {
			login: function (userID, userPassword, successCallback,
				errorCallback) {

				var headerObj = {};
				headerObj.id = userID;
				headerObj.pw = userPassword;

				return api_post_login(headerObj,
					successCallback,
					errorCallback);
			},
			register: function (userID, userPassword, userName, description,
				successCallback,
				errorCallback) {
				var headerObj = {};
				headerObj.id = userID;
				headerObj.pw = userPassword;
				headerObj.username = userName;
				headerObj.description = description;
				return api_put_register(headerObj,
					successCallback,
					errorCallback);
			},
			checkToken: function (token, successCallback,
				errorCallback) {
				return api_get_tokenValid(token,
					successCallback,
					errorCallback);
			}
		};
	});


})();
