/*jslint node: true */
"use strict";
var email = require("emailjs");

var action = {};

var server = email.server.connect({
	user: "battlemaster002@outlook.com",
	password: "master99k",
	host: "smtp-mail.outlook.com",
	tls: {
		ciphers: "SSLv3"
	}
});

function getDecision(isApproved) {
	if (isApproved) {
		return "Approved";
	} else {
		return "Rejected";
	}
}

action.sendResultEmail = function (d_link, d_decision, d_date, d_expertname,
	callback) {
	var link = d_link;
	var decision = getDecision(d_decision);
	var date = d_date;
	var subject = d_expertname + " is " + decision;
	var message = {
		from: "battlemaster002@outlook.com",
		to: "eric@lynkpeople.com",
		bcc: "iamjb@live.hk",
		subject: subject,
		attachment: [{
			data: "<html><h1>Auto sent email</h1><div>1. URL: " + link +
				"</div><div>2. Result: " + decision + "</div><div>3. Date: " + date +
				"</div></html>",
			alternative: true
		}, ]
	};
	// send the message and get a callback with an error or details of the message that was sent
	server.send(message, function (err, message) {

		if(callback && typeof callback === 'function'){
			callback();
		}
	});
};

module.exports = action;
