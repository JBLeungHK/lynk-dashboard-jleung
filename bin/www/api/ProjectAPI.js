(function () {
	"use strict";

	var ng = angular.module('api.project', ['utilities.restful.api',
		'helper.userData'
	]);

	ng.factory('projectAPI', function ($http, APIRoute, RestfulManager,
		UserDataHelper) {

		var apiPrefix = APIRoute.url + 'project/';

		function createHeaderObj(dataObj) {
			var headerObj = null;
			if (dataObj) {
				headerObj = dataObj;
			} else {
				headerObj = {};
			}
			headerObj['lynk-access-token'] = UserDataHelper.getToken();
			return headerObj;
		}


		//implement api
		function api_post_createProject(headerObj, successCallback, errorCallback) {
			var url = apiPrefix + 'createProject';
			var headerData = createHeaderObj(headerObj);
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};
			RestfulManager.post().url(url).header(headerData).requestSend(
				apiCallback,
				failCallback);
		}

		function api_get_projectList(headerObj, successCallback, errorCallback) {
			var url = apiPrefix + 'list';
			var headerData = createHeaderObj(headerObj);
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};
			RestfulManager.get().url(url).header(headerData).requestSend(
				apiCallback,
				failCallback);
		}

		function api_get_projectByID(headerObj, queryObj, successCallback,
			errorCallback) {
			var url = apiPrefix + 'public/get';
			var headerData = createHeaderObj(headerObj);
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};
			RestfulManager.get().url(url).header(headerData).queryParams(queryObj).requestSend(
				apiCallback,
				failCallback);
		}

		function api_put_projectDecision(headerObj, successCallback,
			errorCallback) {
			var url = apiPrefix + 'projectDecision';
			var headerData = createHeaderObj(headerObj);
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};
			RestfulManager.put().url(url).header(headerData).requestSend(
				apiCallback,
				failCallback);
		}

		function api_get_projectAcionHistory(queryObj, successCallback,
			errorCallback) {
			var url = apiPrefix + 'getHistory';
			var headerData = createHeaderObj();
			var apiCallback = function (responseData, status) {
				return successCallback(responseData, status);
			};
			var failCallback = function (responseData, status) {
				if (errorCallback) {
					errorCallback(responseData, status);
				}
			};
			RestfulManager.get().url(url).header(headerData).queryParams(queryObj).requestSend(
				apiCallback,
				failCallback);
		}



		//interface for api call
		return {
			createProject: function (title, startdate, successCallback,
				errorCallback) {
				var headerObj = {
					'title': title,
					'startdate': startdate
				};
				return api_post_createProject(headerObj,
					successCallback,
					errorCallback);
			},
			getProjectList: function (successCallback,
				errorCallback) {
				return api_get_projectList(null,
					successCallback,
					errorCallback);
			},
			getProjectByID: function (id, successCallback,
				errorCallback) {
				var queryObj = {
					'id': id
				};
				return api_get_projectByID(null, queryObj,
					successCallback,
					errorCallback);
			},
			updateProjectDecision: function (projectID, projectName, expertname,
				decision,
				userName,
				decisionID,
				link,
				successCallback) {
				var headerObj = {
					'id': projectID,
					'userid': userName,
					'projectname': projectName,
					'expertname': expertname,
					'decision': decision,
					'decisionid': decisionID,
					'link': link
				};
				api_put_projectDecision(headerObj, successCallback);
			},
			getUserActionHistory: function (sortColumn, order, successCallback,errorCallback) {
				var queryObj = {
					'user_id': UserDataHelper.getUserName(),
				};
				if (sortColumn) {
					queryObj.sort = sortColumn;
					queryObj.order = order;
				}
				api_get_projectAcionHistory(queryObj, successCallback,errorCallback);
			}
		};
	});
})();
