/*jslint node: true */
"use strict";

var mongoHelper = require('../helper/mongooseHelper');
var action = {};

// init model
var collectionName = 'history';
var historySchema = mongoHelper.Schema({
	'user_id': String,
	'action_type': String,
	'action_project': String,
	'action_data': String,
	'action_date': Date
});
var _Model = mongoHelper.model(collectionName, historySchema);
var action = {};

function createModel(id, action, project, data, callback) {
	return new _Model({
		user_id: id,
		action_type: action,
		action_project: project,
		action_data: data,
		action_date: new Date()
	});
}

function addHisory(id, action, project, data, callback) {
	var history = createModel(id, action, project, data);
	try {
		mongoHelper.connect(history, function (saveModel) {
			saveModel.save(function (err, result) {
				if (err) {
					return console.error(err);
				}
				if (callback && typeof callback === 'function') {
					callback(result);
				}
			});
		});
	} finally {
		mongoHelper.afterResponse();
	}
}

function queryHistoryListByUserID(id, sortColumn, order, callback) {
	var dataJson = null;
	if (id) {
		dataJson = {};
		dataJson.user_id = id;
	}
	try {
		mongoHelper.connect(dataJson, function (data) {
			var query = _Model.find(data);
			if (sortColumn) {
				var orderKey = sortColumn;
				if (!order) {
					orderKey = '-' + orderKey;
				}
				query.sort(orderKey);
			}
			query.exec(function (err, projects) {
				if (callback && typeof callback === 'function') {
					return callback(projects);
				}
			});
		});
	} finally {
		mongoHelper.afterResponse();
	}
}

action.getHistoryList = function (id, sortColumn, order, callback) {
	queryHistoryListByUserID(id, sortColumn, order, callback);
};

action.addHistory = function (id, action, project, data, callback) {
	addHisory(id, action, project, data, callback);
};

module.exports = action;
