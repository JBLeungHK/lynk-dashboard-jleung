/*jslint node: true */
"use strict";

var express = require('express');
var userModel = require('../../lib/js/model/userModel');

var router = express.Router();

/* GET project listing. */
router.get('/getusers', function (req, res, next) {
	var ids = req.query.ids;
	if (ids) {
		ids = ids.split(',');
	}
	var callback = function (data) {
		res.json(data);
	};
	userModel.getUserlist(ids, callback);
});


module.exports = router;
