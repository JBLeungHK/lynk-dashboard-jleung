/*jslint node: true */
"use strict";

var express = require('express');
var extend = require('util')._extend;
var projectModel = require('../../lib/js/model/projectModel');
var userModel = require('../../lib/js/model/userModel');
var historyModel = require('../../lib/js/model/historyModel');
var email = require('../../lib/js/helper/emailHelper');

var router = express.Router();

function addHistory(id, actionType, actionData, expertname, callback) {
	historyModel.addHistory(id, actionType, actionData, expertname, callback);
}

/* GET project listing. */
router.get('/list', function (req, res, next) {
	var callback = function (data) {
		res.json(data);
	};
	projectModel.getProjects(callback);
});

router.put('/projectDecision', function (req, res, next) {
	var projectID = req.headers.id;
	var projectName = req.headers.projectname;
	var expertname = req.headers.expertname;
	var userID = req.headers.userid;
	var decision = JSON.parse(req.headers.decision);
	var decisionID = req.headers.decisionid;
	var link = req.headers.link;
	var callback = function (data) {
		console.log("1st callback in projectDecision");
		if (data.ok === 1) {
			addHistory(userID, decision, projectName, expertname, function (data,
				status) {
				console.log("2nd callback in projectDecision");
				email.sendResultEmail(link, decision, new Date(), expertname, function () {
					console.log("3rd callback in projectDecision");
				});
				res.send(data);
			});
		}
	};
	if (decision) {
		projectModel.userAcceptProejct(projectID, userID, decisionID, callback);
	} else {
		projectModel.userRejectProject(projectID, userID, decisionID, callback);
	}
});


router.post('/createProject', function (req, res, next) {
	var title = req.headers.title;
	var startDate = new Date(req.headers.startdate);
	var callback = function (data) {
		res.send('added project: ' + data.title);
	};
	projectModel.addProjects(title, startDate, callback);
});

router.get('/getHistory', function (req, res, next) {
	var id = req.query.user_id;
	var sortColumn = req.query.sort;
	var order = JSON.parse(req.query.order);
	historyModel.getHistoryList(id, sortColumn, order, function (
		data, state) {
		res.json(data);
	});
});

module.exports = router;
