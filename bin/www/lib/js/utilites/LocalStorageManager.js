(function () {

    var ng = angular.module('utilities.localStorage', []);

    //private variables
    var userDataKey = "userData";
    var persistRecords = [userDataKey];
    var recordsKey = 'recordsKey';
    var records = angular.fromJson(window.localStorage[recordsKey] || []);

    //private function
    function getRecord(key) {
        var record = angular.fromJson(window.localStorage[key]);
        if (record) {
            return record;
        }
        return undefined;
    }

    function getValueFromRecord(key, valueKey) {
        var record = getRecord(key);
        if (record && record.hasOwnProperty(valueKey)) {
            return getRecord(key)[valueKey];
        }
        return undefined;
    }

    function setRecord(key, object) {
        persist(key, object);
        if (isPersist(key)) {
            records.push(key);
            persist(recordsKey, records);
        }
    }

    function addValueToRecord(key, valueKey, value) {
        var update = getRecord(key);
        if (update) {
            update[valueKey] = value;
            persist(key, update);
        } else {
            console.log("key \"" + key + "\" not exisit");
        }
    }

    function delectRecord(key) {
        if (getRecord(key)) {
            window.localStorage.removeItem(key);
            if (!isPersist(key)) {
                records.splice(records.indexOf(key), 1);
                persist(recordsKey, records);
            }
        }
    }

    function delectRecordAll() {
        for (var i = 0; i < records.length; i++) {
            delectRecord(records[i]);
        }
        for (var i = 0; i < persistRecords.length; i++) {
            delectRecord(persistRecords[i]);
        }
    }

    function persist(key, object) {
        window.localStorage[key] = angular.toJson(object);
    }

    function isPersist(key) {
        return (!(records.indexOf(key) > -1) && !(persistRecords.indexOf(key) > -1));
    }

    function clearNorPersist() {

        for (var i = 0; i < records.length; i++) {
            delectRecord(records[i]);
        }
    }

    function printValuseByKey(prefix, array) {
        for (var i = 0; i < array.length; i++) {
            var key = array[i];
        }
    }


    ng.factory('LocalStorageHelper', function () {

        clearNorPersist();
//        delectRecordAll();

        //public function
        return {
            getRecord: function (key) {
                return getRecord(key);
            },
            getValueFromRecord: function (key, valueKey) {
                return getValueFromRecord(key, valueKey);
            },
            setRecord: function (key, object) {
                setRecord(key, object);
            },
            addValueToRecord: function (key, valueKey, value) {
                addValueToRecord(key, valueKey, value);
            },
            getUserDataKey: function(){
                return userDataKey;
            }
        };

    });
}());
