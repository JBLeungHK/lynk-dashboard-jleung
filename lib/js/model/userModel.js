/*jslint node: true */
"use strict";

var mongoHelper = require('../helper/mongooseHelper');
var passwordHelper = require('../helper/passwordHelper');
var action = {};

// init model
var collectionName = 'accounts';
var userSchema = mongoHelper.Schema({
	user_id: String,
	password: String,
	name: String,
	description: String,
	create_date: Date
});
var _Model = mongoHelper.model(collectionName, userSchema);

function createModel(id, pw, username, destpion) {
	return new _Model({
		user_id: id,
		password: pw,
		name: username,
		description: destpion,
		create_date: new Date()
	});
}

//create User function
var createUserCallback = function createUser(data, id, pw, username,
	description, callback) {
	if (!data.result) {
		var password = passwordHelper.encryptPW(pw);
		var user = createModel(id, password, username, description);
		try {
			mongoHelper.connect(user, function (saveModel) {
				saveModel.save(function (err, result) {
					if (err) {
						return console.error(err);
					}
					if (callback && typeof callback === 'function') {
						callback({
							'user': saveModel.user_id,
							'result': true
						});
						console.log('insert user success: ' + JSON.stringify(
							saveModel));
					}
				});
			});
		} finally {
			mongoHelper.afterResponse();
		}
	} else {
		if (callback && typeof callback === 'function') {
			callback({
				'result': false
			});
		}
		console.error('user ' + id + ' is exisit');
	}
};

//query user by id or pw
function queryUserByIdPw(ids, pw, username, description, queryField, callback,
	isCheckPW) {
	var dataJson = {};
	if (ids) {
		dataJson.id = ids;
	}
	if (pw) {
		dataJson.pw = pw;
	}
	if (username) {
		dataJson.username = username;
	}
	if (description) {
		dataJson.description = description;
	}
	if (isCheckPW) {
		dataJson.isCheckPW = isCheckPW;
	}
	if (queryField) {
		dataJson.queryField = queryField;
	}
	try {
		mongoHelper.connect(dataJson, function (data) {
			var query = null;
			if (dataJson.id && Array.isArray(dataJson.id)) {
				query = _Model.find({
					'user_id': {
						$in: dataJson.id
					}
				});
			} else {
				query = _Model.findOne({
					user_id: data.id
				});
			}
			if (data.isCheckPW) {
				query.where('password').equals(passwordHelper.encryptPW(data.pw));
			}

			if (data.queryField) {
				query.select(data.queryField);
			}

			query.exec(function (err, users) {
				if (err) {
					console.error('login fail: ' + data.id);
				}
				var result = {};
				if (users) {
					if (Array.isArray(users)) {
						var updatedList = [];
						users.forEach(function(value, index, array){
							var udpateValue = value.toJSON();
							delete udpateValue.password;
							updatedList.push(udpateValue);
						});
						result = updatedList;
					} else {
						users = users.toJSON();
						delete users.password;
						result = users;
					}
					result.result = true;
				} else {
					result.result = false;
				}

				if (callback && typeof callback === 'function') {
					return callback(result, data.id, data.pw, data.username, data.description);
				}
				return result;
			});

		});
	} finally {
		mongoHelper.afterResponse();
	}
}

//start action implement
action.createUser = function (id, pw, username, description, callback) {
	if (id !== '' && pw !== '') {
		var dataJson = {
			'id': id,
			'pw': pw,
			'username': username,
			'description': description
		};
		var cb = null;
		if (callback && typeof callback === 'function') {
			cb = function (data, id, pw, username, description) {
				createUserCallback(data, id, pw, username, description, callback);
			};
		}
		mongoHelper.connect(dataJson, function (data) {
			queryUserByIdPw(data.id, data.pw, data.username, data.description,
				'user_id',
				cb, false);
		});
	} else {
		return false;
	}
};

action.login = function (id, pw, callback) {
	queryUserByIdPw(id, pw, null, null, 'user_id', callback, true);
};

action.getUserlist = function (ids, callback) {
	queryUserByIdPw(ids, null, null, null, null, callback, false);
};

module.exports = action;
