(function () {
  'user strict';

  var ng = angular.module('utilities.restful.api', []);

  ng.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($rootScope) {
      return {
        request: function (config) {
          return config;
        },
        response: function (response) {
          return response;
        }
      };
    });
  });

  //private variables
  //private function
  function setData(data, configObj) {
    if (data) {
      configObj.data = data;
    }
    return configObj;
  }

  function setHeader(data, configObj) {
    if (data) {
      configObj.headers = data;
    }
    return configObj;
  }

  function setMethod(method, configObj) {
    if (method && method.length > 0) {
      configObj.method = method;
    }
    return configObj;
  }

  function setUrl(url, configObj) {
    if (url && url.length > 0) {
      configObj.url = url;
    }
    return configObj;
  }

  function setParams(data, configObj) {
    if (data) {
      configObj.params = data;
    }
    return configObj;
  }

  function request($http, configObj, successCallback, errorCallback) {

    configObj.timeout = 1000 * 10;

    $http(configObj).
    then(function (response) {
      if (successCallback) {
        successCallback(response.data, response.status);
      }
    }, function (data, status, headers, config) {
      if (!errorCallback) {
        console.error("Network Error", "OK",
          "Network is not stable, try again later");
      } else {
        errorCallback(data, status, headers, config);
      }

    });
  }

  ng.factory('RestfulManager', function ($rootScope, $http) {

    var configObj = null;

    //public function
    return {
      data: function (value) {
        setData(value, configObj);
        return this;
      },
      header: function (value) {
        setHeader(value, configObj);
        return this;
      },
      url: function (value) {
        setUrl(value, configObj);
        return this;
      },
      queryParams: function (value) {
        setParams(value, configObj);
        return this;
      },
      get: function () {
        configObj = {};
        setMethod('GET', configObj);
        return this;
      },
      post: function () {
        configObj = {};
        setMethod('POST', configObj);
        return this;
      },
      put: function () {
        configObj = {};
        setMethod('PUT', configObj);
        return this;
      },
      requestSend: function (successCallback, errorCallback) {
        request($http, configObj, successCallback, errorCallback);
      }
    };

  });
}());
