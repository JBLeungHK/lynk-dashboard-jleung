/*jslint node: true */
"use strict";

var mongoose = require('mongoose');

var dbname = "jeffleunglynkdb";
var dbURI = 'mongodb://localhost/' + dbname;
// var dbname = "lynkdashboard";
var helper = {};

var db = null;

function getConnection(data, callback) {
  if (!getConnectionStatus()) {
    mongoose.connect(dbURI);
    db = mongoose.connection;
    db.on('error', console.error.bind(console, 'mongo connection error:'));
    db.on('open', function() {
      connectedCallback(data, callback);
    });
  } else {
    connectedCallback(data, callback);
  }
}

function connectedCallback(data, callback) {
  if (typeof callback === 'function') {
    callback(data);
  }
}

function getConnectionStatus(){
  return mongoose.connection.readyState;
}

helper.afterResponse = function() {
  if (!getConnectionStatus()) {
    mongoose.connection.close(function() {
      db = null;
      console.log('Mongoose connection disconnected');
    });
  }
};

helper.connect = function(dataJson, callback) {
  getConnection(dataJson, callback);
};

helper.Schema = function(json) {
  return mongoose.Schema(json);
}

helper.model = function(key, schema) {
  return mongoose.model(key, schema);
}
helper.getConnectionStatus = function(){
  return getConnectionStatus();
}

module.exports = helper;
