/*jslint node: true */
"use strict";

var mongoHelper = require('../helper/mongooseHelper');
var action = {};

// init model
var collectionName = 'projects';
var projectSchema = mongoHelper.Schema({
	'title': String,
	'start_date': Date,
	'status': String,
	'status_order': Number,
	'decision_list': [{
		user_id: String,
		decision: String,
		user_name: String,
		description: String
	}],
});
var _Model = mongoHelper.model(collectionName, projectSchema);
var action = {};

function createModel(name, startDate) {
	return new _Model({
		title: name,
		start_date: startDate,
		status: 'new'
	});
}

//get projects
function queryProject(id, isIDQuery, callback) {
	var dataJson = null;
	if (id && id.length > 0) {
		dataJson = {};
		dataJson._id = id;
	} else if (isIDQuery) {
		console.log("should have ids");
		callback(false);
		return;
	}
	try {
		mongoHelper.connect(dataJson, function (data) {
			if (dataJson) {
				_Model.findOne(dataJson, function (err, projects) {
					if (callback && typeof callback === 'function') {
						return callback(projects);
					}
				});
			} else {
				var query = _Model.find();
				query.sort('status_order -start_date -title');
				query.exec(function (err, projects) {
					if (callback && typeof callback === 'function') {
						return callback(projects);
					}
				});
			}
		});
	} finally {
		mongoHelper.afterResponse();
	}
}

function addProjects(title, startDate, callback) {
	if (title && title !== '' && startDate) {
		var project = createModel(title, startDate);
		try {
			mongoHelper.connect(project, function (saveModel) {
				saveModel.save(function (err, result) {
					if (err) {
						return console.error(err);
					}
					if (callback && typeof callback === 'function') {
						callback(result);
					}
				});
			});
		} finally {
			mongoHelper.afterResponse();
		}
	} else {
		console.error("add project error, input value error");
	}
}

//add accept user
function updateUserDecision(projectID, userID, decision, decisionID, callback) {
	// var dataJson = {};
	// dataJson.projectID = projectID;
	// dataJson.user_id = userID;
	// dataJson.decision = decision;
	// dataJson.decisionID = decisionID;
	try {
		mongoHelper.connect(null, function () {

			var decisionObj = {};
			decisionObj.user_id = userID;
			decisionObj.decision = decision;
			console.log("check decisionID: " + decisionID);
			if (decisionID !== undefined) {
				decisionObj._id = decisionID;
				console.log("hihihihi3: " + decisionID);
				_Model.update({
						'decision_list._id': decisionID
					}, {
						'$set': {
							'decision_list.$.decision': decision,
						}
					},
					function (err, model) {
						if (err) {
							console.log(err);
							callback(model);
						}
						console.log("decision udpate result");
						console.log(model);
						callback(model);
					});
			} else {
				_Model.findByIdAndUpdate(
					projectID, {
						$push: {
							"decision_list": decisionObj
						}
					}, {
						safe: true,
						upsert: true
					},
					function (err, model) {
						if (err) {
							console.log(err);
							callback(model);
						}
						callback(model);
					});
			}



		});
	} finally {
		mongoHelper.afterResponse();
	}

}

//remove accept user

//add reject user

//remove reject user

//public action
action.getProjects = function (callback) {
	queryProject(null, false, callback);
};
action.getProjectByID = function (id, callback) {
	queryProject(id, true, callback);
};
action.addProjects = function (title, startDate, callback) {
	addProjects(title, startDate, callback);
};
action.userAcceptProejct = function (productID, userID, decisionID, callback) {
	updateUserDecision(productID, userID, 'approve', decisionID,
		callback);
};
action.userRejectProject = function (productID, userID, decisionID, callback) {
	updateUserDecision(productID, userID, 'reject', decisionID,
		callback);
};

module.exports = action;
