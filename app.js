/*jslint node: true */
"use strict";

//start api server
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

//list of api js
var jwtManager = require('./routes/jwtManager');
var checkHealth = require('./routes/api/checkHealth');
var auth = require('./routes/api/auth');
var user = require('./routes/api/user');
var project = require('./routes/api/project');
var publicProject = require('./routes/api/publicProject');
// var email = require('./routes/api/email');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));

//define routes for apis


app.use('/api/checkHealth', checkHealth);
app.use('/api/auth/checkToken', jwtManager.jwtVerify);
app.use('/api/project/public', publicProject);
app.use('/api/auth', auth);
app.use('/api/*', jwtManager.jwtVerifyToNext);
app.use('/api/user', user);
app.use('/api/project', project);
// app.use('/api/email', email);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});


module.exports = app;


//start http server
var connect = require('connect');
var serveStatic = require('serve-static');

var port = 8080;
var rootDir = "./bin/www";

connect().use(serveStatic(rootDir)).listen(port, function () {
	console.log('Server running on ' + port);
});
