/*jslint node: true */
"use strict";
var express = require('express');
var userModel = require('../../lib/js/model/userModel');
var router = express.Router();
var jwtManager = require('../jwtManager');

// PUT: create user
router.put('/createUser', function (req, res, next) {
	var id = req.headers.id;
	var pw = req.headers.pw;
	var username = req.headers.username;
	var description = req.headers.description;
	userModel.createUser(id, pw, username, description, function (data) {
		res.send(data);
	});
});

router.post('/login', function (req, res, next) {
	var id = req.headers.id;
	var pw = req.headers.pw;
	userModel.login(id, pw, function (data) {
		if (data.result) {
			var token = jwtManager.creatToken(data);
			data.token = token;
		}
		res.json(data);
	});
});

module.exports = router;
