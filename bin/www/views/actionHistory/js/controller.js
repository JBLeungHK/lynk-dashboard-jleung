(function () {
	"use strict";

	var ng = angular.module('view.actionhistory', ['api.project',
		'helper.userData'
	]);

	//router config
	ng.config(function ($stateProvider) {
		$stateProvider
			.state('actionHistory', {
				url: "/history",
				templateUrl: "views/actionHistory/index.html",
				controller: "ActionHistoryController"
			});
	});

	// setup contoller
	ng.controller('ActionHistoryController', ActionHistoryController);


	var historyTableHeader = [];
	historyTableHeader.push({
		'key': 'action_data',
		'value': 'Expert Name'
	});
	historyTableHeader.push({
		'key': 'action_project',
		'value': 'Project Title'
	});
	historyTableHeader.push({
		'key': 'action_type',
		'value': 'Approved/Rejected'
	});
	historyTableHeader.push({
		'key': 'action_date',
		'value': 'Date'
	});


	//implement contoller
	function ActionHistoryController($scope, $rootScope, $location, projectAPI) {

		//init scope function
		$rootScope.mainTitle = "History";
		//init header
		$scope.tableHeaders = historyTableHeader;
		$scope.masterOrderKey = null;

		// //query history content
		projectAPI.getUserActionHistory('action_date', true, function (data, status) {
			$scope.historyList = data;
		},function(){
			$location.path("/");
		});

		$scope.sorting = function(index,sortColumn, order){
			$scope.masterOrderKey = index;
			projectAPI.getUserActionHistory(sortColumn, order, function (data, status) {
				$scope.historyList = data;
			});
		};
	}


}());
