/*jslint node: true */
"use strict";

var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var inject = require('gulp-inject');

var paths = {
  javascript: [
    './bin/www/views/*/js/*.js',
    './bin/www/lib/js/*/*.js',
    './bin/www/api/*.js'
  ],
  css: [
    './bin/www/views/*/css/*.css'
  ]
};

gulp.task('index', function() {
  return gulp.src('./bin/www/index.html')
    .pipe(inject(
      gulp.src(paths.javascript, {
        read: false
      }), {
        relative: true
      }))
    .pipe(inject(
      gulp.src(paths.css, {
        read: false
      }), {
        relative: true
      }))
    .pipe(gulp.dest('./bin/www'));
});

gulp.task('default', ['index']);

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
