(function () {
	"use strict";

	var ng = angular.module('view.login', ['api.auth', 'ngAnimate',
		'ui.bootstrap', 'helper.userData'
	]);

	//router config
	ng.config(function ($stateProvider) {
		$stateProvider
			.state('login', {
				url: "/",
				templateUrl: "views/login/index.html",
				controller: "LoginController"
			})
			.state('registerPage', {
				url: "/signup",
				templateUrl: "views/login/register.html",
				controller: "RegisterController"
			});
	});

	//setup contoller
	ng.controller('LoginController', LoginController);
	ng.controller('RegisterController', RegisterController);
	ng.controller('LoginPopupContoller', LoginPopupContoller);
	ng.controller('RegisterPopupContoller', RegisterPopupContoller);

	// LoginController.$inject = ['$scope', '$location', 'authAPI'];

	function LoginController($scope, $rootScope, $state, $location, authAPI,
		$uibModal, UserDataHelper) {

		$rootScope.mainTitle = "Login";

		var token = UserDataHelper.getToken();
		if (token) {
			authAPI.checkToken(token, function (data, staus) {
				if (data) {
					$rootScope.username = UserDataHelper.getUserName();
					if ($state.current.name === 'login') {
						$location.path('/dashboard');
					} else {
						$location.path('/');
						UserDataHelper.clearAll();
					}
				} else {
					clearAllCachedUserData();
					$location.path('/');
					UserDataHelper.clearAll();
				}
			});
		} else {
			clearAllCachedUserData();
		}

		function clearAllCachedUserData() {
			UserDataHelper.clearAll();
			$rootScope.username = null;
		}

		$scope.login = function (id, pw) {
			authAPI.login(id, pw, function (data, status) {
				var modalInstance = $uibModal.open({
					templateUrl: 'LoginPopup.html',
					controller: 'LoginPopupContoller',
					resolve: {
						data: function () {
							return data;
						}
					}
				});
			});

		};

	}

	// RegisterController.$inject = ['$scope', '$location', 'authAPI', '$uibModal',
	// 	'$log'
	// ];

	function RegisterController($scope, $location, authAPI, $uibModal, $log) {

		$scope.animationsEnabled = true;
		$scope.register = function (id, pw, userName, description) {
			authAPI.register(id, pw, userName, description, function (data, status) {
				var modalInstance = $uibModal.open({
					templateUrl: 'RegisterPopup.html',
					controller: 'RegisterPopupContoller',
					resolve: {
						data: function () {
							return data;
						}
					}
				});
			});
		};
	}

	function LoginPopupContoller($scope, $rootScope, $location, $uibModalInstance,
		UserDataHelper, data) {
		if (data.result) {
			$scope.message = 'Login Success';
		} else {
			$scope.message = 'Login Fail, please try again';
		}
		$scope.done = function () {
			$uibModalInstance.close();
			if (data.result) {
				UserDataHelper.setToken(data.token);
				UserDataHelper.setUserName(data.user_id);
				$rootScope.username = UserDataHelper.getUserName();
				$location.path('/dashboard');
			}
		};
	}

	function RegisterPopupContoller($scope, $location, $uibModalInstance, data) {
		if (data.result) {
			$scope.message = 'Account is created, will go to login page';
		} else {
			$scope.message = 'Register fail, please try again';
		}
		$scope.done = function () {
			$uibModalInstance.close();
			if (data.result) {
				$location.path('/');
				UserDataHelper.clearAll();
			}
		};
	}
}());
