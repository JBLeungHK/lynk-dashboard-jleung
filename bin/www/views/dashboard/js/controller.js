(function () {
	"use strict";

	var ng = angular.module('view.dashboard', ['api.project', 'api.user',
		'helper.userData'
	]);

	//router config
	ng.config(function ($stateProvider) {
		$stateProvider
			.state('dashboard', {
				url: "/dashboard",
				templateUrl: "views/dashboard/index.html",
				controller: "DashboardController"
			})
			.state('singleProject', {
				url: "/project?id",
				templateUrl: "views/dashboard/singleProject.html",
				controller: "SingleProjectController"
			});
	});

	// setup contoller
	ng.controller('DashboardController', DashboardController);
	ng.controller('SingleProjectController', SingleProjectController);


	var projectTableHeader = [];
	projectTableHeader.push({
		'key': 'title',
		'value': 'Title'
	});
	projectTableHeader.push({
		'key': 'start_date',
		'value': 'Start Date'
	});
	projectTableHeader.push({
		'key': 'status',
		'value': 'Status'
	});
	var singleProjectTableHeader = [];
	singleProjectTableHeader.push({
		'key': 'user_name',
		'value': 'Full Name'
	});
	singleProjectTableHeader.push({
		'key': 'description',
		'value': 'Description'
	});
	singleProjectTableHeader.push({
		'key': 'decision',
		'value': 'Status'
	});


	//implement contoller
	function DashboardController($scope, $rootScope, $location, projectAPI) {

		$rootScope.mainTitle = "Projects Dashboard";

		//init scope function
		$scope.goToSingleProjectPage = function (id) {
			console.log(id);
			$location.path('/project?id');
		};

		//init header
		$scope.tableHeaders = projectTableHeader;

		//query project content
		projectAPI.getProjectList(function (data, status) {
			$scope.projectlist = data;
		}, function () {
			$location.path("/");
		});
	}

	function SingleProjectController($scope, $rootScope, $location, $stateParams,
		projectAPI,
		userAPI, UserDataHelper) {

		$rootScope.mainTitle = "Project Detail";

		$scope.singleProjectTableHeader = singleProjectTableHeader;

		//get project detail list
		projectAPI.getProjectByID($stateParams.id, function (data, status) {
			if (data === "") {
				$location.path('/');
				UserDataHelper.clearAll();
				return;
			} else {
				$scope.projectTitle = data.title;
				$scope.decisionlist = data.decision_list;
			}
		}, function () {
			$location.path('/');
			UserDataHelper.clearAll();
		});

		//implement scope function
		$scope.projectDecision = function (decisionId, decision,
			userName) {
			projectAPI.updateProjectDecision($stateParams.id, $scope.projectTitle,
				userName, decision,
				UserDataHelper.getUserName(),
				decisionId,
				window.location.href,
				function (data, status) {

				});
		};

	}
}());
