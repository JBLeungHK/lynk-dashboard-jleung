
echo "====================================\nstart install npm require packages";
npm install;
echo "npm install end=\n";
echo "====================================\nstart install bower require packages";
bower install;
echo "bower install end=\n";
echo "====================================\nstart import data to monogoDB-accounts";
mongoimport --db jeffleunglynkdb --collection accounts --type csv --headerline --file ./importData/accounts.csv;
echo "mongoDB import-accounts=\n";
echo "====================================\nstart import data to monogoDB-projects";
mongoimport --db jeffleunglynkdb --collection projects --type json --file ./importData/projects.json;
echo "mongoDB import-porjects=\n";
