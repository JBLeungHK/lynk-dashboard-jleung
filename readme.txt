Hi Eric,

Here is the result system.
And there are some assumptions and how to get start:

--Assumptions:
Warning!!!! I user outlook.com stmp server, so sometime will be block because it like spam email. But the email function is work well if the smtp functional.

0. develop and tested in Mac env only, but must be ok in window as well
1. MongoDB and Nodejs is well install in the running env.

2. front-end & back-end implement in one nodejs project because more easy use only,
and the front end and backend is separated no js internal calling, all connect with RESTFUL API.
  -front end: angularJS, UI.Router, Bootstrap 3, bower, gulp
  -back end: nodejs, express, mongoose, JWT(jsonwebtoken)

3. All new create users are admin and has full right to approve and reject all projects and all experts.

4. date display as 'dd/MM/yyyy' rather than 'dd MM yyyy' which is more user friendly.

5. user account email must be unique in the system.

6.Programming Detail:
	front end:
		a. root dir is ./bin/www
		b. each view grouped in a view folder ./bin/www/views/*
		c. all api call handler in folder ./bin/www/api/*
  back-end:
    a.use jwtManager to handle jwt verification.
    b.api interface implement in folder ./routes/api/*
    c.db model in folder ./lib/js/model/*

--Get start:
(assume can access local mongoDB without id and pw)
1. run init.sh in the cmd
    "sh ./init.sh"
2. start the api server and web server with command
    "npm start"
3. entry point: localhost:8080
4.Enjoy!

PS: The package include a postman collection you may hv a look. But most of the api need access_token, so u may not call it easily.
