(function () {
	'use strict';
	var app = angular.module('mainApp', ['ui.router',
		'helper.userData',
		'api.auth',
		'view.login',
		'view.dashboard',
		'view.actionhistory'
	]);

	app.constant('APIRoute', {
		url: 'http://localhost:3001/api/'
	});

	app.config(function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');
	});


	app.controller('mainViewsController', function ($scope, $rootScope, $state,
		$location, UserDataHelper) {
		$rootScope.username = UserDataHelper.getUserName();

		//implement scope function
		$scope.logout = function () {
			UserDataHelper.clearAll();
			$location.path('/');
		};


	});

}());
